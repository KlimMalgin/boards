import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

import store from './store/store.js';
import Viewport from './components/viewport/viewport.vue';


Vue.use(BootstrapVue);

new Vue( {
    el     : '#app',
    store  : store,
    render : h => h( Viewport ),
} );

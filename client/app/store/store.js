import Vue from 'vue';
import Vuex from 'vuex';

Vue.use( Vuex );

const
    ADD_COLUMN = 'ADD_COLUMN',
    REMOVE_COLUMN = 'REMOVE_COLUMN',
    EDIT_COLUMN_ITEM = 'EDIT_COLUMN_ITEM',
    MERGE_COLUMN_ITEM = 'MERGE_COLUMN_ITEM';


const state = {

    columns: [
        {
            index: 1,
            name: 'Went well',
            items: [
                {
                    index: 1,
                    items: [
                        {
                            text: 'hohoho pol',
                        }
                    ]
                },
                {
                    index: 2,
                    items: [
                        {
                            text: "I'll be back",
                        }
                    ]
                },
                {
                    index: 3,
                    items: [
                        {
                            text: 'kopolo',
                        }
                    ]
                },
            ]
        },
        {
            index: 2,
            name: 'To improve',
            items: [
                {
                    index: 3,
                    items: [
                        {
                            text: 'toronto',
                        }
                    ]
                },
                {
                    index: 22,
                    items: [
                        {
                            text: 'chelly',
                        }
                    ]
                },
            ]
        },
        {
            index: 3,
            name: 'Action items',
            items: [
                {
                    index: 1,
                    items: [
                        {
                            text: 'trotrrro',
                        }
                    ]
                },
                /*{
                    index: 3,
                    text: 'pp ppp toronto',
                    items: []
                },*/
            ]
        },
    ],

};

const mutations = {

    [ADD_COLUMN]( state, payload ) {
        state.columns[payload.key].items.push(payload.value);
    },
    [REMOVE_COLUMN]( state, payload ) {
        // payload.columnKey
        // payload.itemKey

    },
    [EDIT_COLUMN_ITEM](state, payload) {
        state.columns[payload.columnKey].items[payload.itemKey].text = payload.value;
    },
    [MERGE_COLUMN_ITEM] (state, payload) {
        /*state.columns[payload.columnKey].items[payload.itemKey].items.push({
            text: payload.value
        });*/
        //debugger;
        if (payload.isSource) {
            state.columns[payload.columnKey].items.splice(payload.itemKey, 1);
        } else {
            state.columns[payload.columnKey].items[payload.itemKey].items = payload.value;
        }
    }

};

const actions = {

    addColumn : ( { commit }, key ) => {
        commit(ADD_COLUMN, {
            key: key,
            value: {
                index: 2,
                text: ''
            }
        });
    },
    removeColumn : ( { commit, state, dispatch }, params ) => {
        commit(REMOVE_COLUMN, params);
    },
    editColumnItem : ({commit}, params) => {
        commit(EDIT_COLUMN_ITEM, params);
    },
    mergeColumnItem: ( {commit}, params ) => {
        commit(MERGE_COLUMN_ITEM, params);
    }

};

const getters = {
    columns: ( state ) => state.columns,
    columnItems: (state) => {
        return (columnKey, itemKey) => {
            return state.columns[columnKey].items[itemKey].items
        };
    },
    columnItem : ( state ) => {
        return (columnKey, itemKey, index=0) => {
            return state.columns[columnKey].items[itemKey].items[index].text
        };
    }
};

let storeObjects = {
    state,
    mutations,
    actions,
    getters,

    modules : {},
};

let store = new Vuex.Store( {
    state,
    mutations,
    actions,
    getters,

    modules : {},
} );

export default store;

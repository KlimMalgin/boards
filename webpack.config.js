const path              = require('path');
const os                = require('os');
const glob              = require('glob');
const HappyPack         = require('happypack');
const webpack           = require('webpack');
const UglifyJSPlugin    = require('uglifyjs-webpack-plugin');

var options = {};

module.exports = {
    entry : {
        boards   : path.resolve( __dirname, 'client/app/boards.js' ),
    },
    output : {
        path       : path.resolve( __dirname, './client/dist/' ),
        publicPath : '/dist/',
        filename   : '[name].js',
    },
    module : { rules : [
        {
            test    : /\.vue$/,
            loader  : 'vue-loader',
            options : { loaders : {
                'less' : 'vue-style-loader!css-loader!less-loader',
                'css'  : 'style-loader!css-loader',
            } },
        },

        {
            test   : /\.css$/,
            loader : 'style-loader!css-loader',
        },

        {
            test   : /\.less$/,
            loader : 'style-loader!css-loader!less-loader',
        },

        {
            test    : /\.js$/,
            loader  : 'babel-loader',
            exclude : /node_modules/,
        },
        {
            test    : /\.(png|jpg|gif|svg)$/,
            loader  : 'file-loader',
            options : { name: '[name].[ext]?[hash]' },
        },
    ] },
    resolve : {
        unsafeCache : true,
        modules     : [
            path.resolve('./node_modules'),
        ],
    },
    devServer : {
        historyApiFallback : true,
        noInfo             : true,
    },
    performance : { hints: false },
    devtool     : '#eval-source-map',
};

if ( process.env.NODE_ENV === 'production' ) {
    options.devtool = '#source-map';
    module.exports.plugins = ( module.exports.plugins || [] ).concat( [
        new webpack.DefinePlugin( { 'process.env' : {
            NODE_ENV : `"${process.env.NODE_ENV}"`,
        } } ),
        new webpack.optimize.UglifyJsPlugin( { sourceMap: options.devtool && options.devtool.indexOf('source-map') >= 0 } ),
    ] );
}
